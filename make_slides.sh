#!/bin/bash
# Requires a relatively recent install of NodeJS
# (recent enough that it comes with npx)
npx @marp-team/marp-cli@latest --html slides.md -o slides_output.pdf
