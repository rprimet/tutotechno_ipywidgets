---
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.svg')
---

# Tutotechno
## Jupyter, Ipywidgets, Binder, et Voilà !

Romain Primet - SED Saclay

2021-11-19

---
# The main idea

**Web-based prototypes on the cheap**

- With easy and automated deployment(s)!
- Especially for tools that require a server-side but no (writable) DB 

## Jupyter?

---
# Ingredients

- **[Jupyter](https://jupyter.org/)** <span style="color:orange">*Interactive computing*</span>
- **[Ipywidgets](https://ipywidgets.readthedocs.io/en/latest/)** <span style="color:orange">*Widgets toolkit*</span>
- **[Binder](https://mybinder.org)** (repo2docker) <span style="color:orange">*Painless ephemeral deployment*</span>
- **[Voilà](https://github.com/voila-dashboards/voila)** <span style="color:orange">*Dashboards / apps*</span>

---
# Examples / case studies

## Motivating example

[`neuroquery_apps`](https://github.com/neuroquery/neuroquery_apps)

## Tutorial example

[`hal2cff`](https://github.com/rprimet/hal2cff)

---
# Set up your tutorial environment

- Assumes a miniforge / mambaforge install

- **Fork** https://github.com/rprimet/hal2cff

```
$ git clone https://github.com/<username>/hal2cff.git && cd hal2cff
$ conda create -n tutotechno python=3.9
$ conda activate tutotechno
$ pip install -r requirements.txt
$ jupyter lab

```

---
# Aside

Look at [direnv](https://direnv.net/) to activate conda environments automatically

---
# Jupyter and Ipywidgets

- A widget kit for Jupyter
- A framework that other libraries build upon

---
# Exercise time!
## <span style="color:orange">*Jupyter + ipywidgets*</span>

Look at `examples/hal2cff_example.py` (but *not* at `basic_ui.py`) and 
create a small ipywidgets UI that contains an address bar, a button to
query HAL and an output area to display the resulting CITATION.cff draft.

**Hint**: you may use `neuroquery_apps` for inspiration.

---
# Binder

- Flexible deploys with little maintenance
- `repo2docker` (inspired by Heroku buildpacks)
- Main drawback: limitations (ephemeral, CPU, memory) on the public instances

---
# Exercise time!
## <span style="color:orange">*Binder*</span>

Run your fork of the repo on Binder.

---
# Voilà

- Turn a notebook into an app


---
# Exercise time!
## <span style="color:orange">*Voilà*</span>

Run your UI as a Voilà dashboard.

---
# Extra credits

Extra credits: 
- **Create an online CFF validator**
- Add a download button or link
- Tools/UIs to wrangle affiliation data, 
- Keywords/topics retrieval, 
- **Search docs with the HAL API directly**,
- UI niceties (spinners...) et caetera

---
# More extra credits...

Stuff that would be nice but not trivial to do (and it's irksome): "copy to clipboard" button, urlparams...

---
# Random neat stuff
## <span style="color:orange">*In no particular order*<span>

- [jupytext](https://github.com/mwouts/jupytext)
- [jupyterlite](https://github.com/jupyterlite/jupyterlite)
- [TLJH](https://github.com/jupyterhub/the-littlest-jupyterhub)

---
# Alternatives

- plotly dash
- streamlit
- comparison [here](https://medium.datadriveninvestor.com/streamlit-vs-dash-vs-voil%C3%A0-vs-panel-battle-of-the-python-dashboarding-giants-177c40b9ea57#8026)

---
# Thanks!

- Discussion / feedback welcome!
- Private Binderhub at Inria?
- [Mattermost channel](https://mattermost.inria.fr/devel/channels/jupyter)

romain.primet@inria.fr
