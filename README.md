Tutotechno: Ipywidgets, Binder, et Voilà!
=========================================

Romain Primet - SED Saclay - Nov 19th, 2021

[Presentation slides](https://gitlab.inria.fr/rprimet/tutotechno_ipywidgets/-/blob/master/slides_output.pdf)
